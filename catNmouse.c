//////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Ashten Akemoto <aakemoto@hawaii.edu>
/// @date    01-20-2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
   int DEFAULT_MAX_NUMBER = 2048;

   printf( "Cat `n Mouse\n" );
   printf( "The number of arguments is: %d\n", argc );
   int customMax;
   int theMaxValue = DEFAULT_MAX_NUMBER;
   if (argc == 2){
      customMax = atoi( argv[1] );
      if(customMax < 1){
         printf("Error! Max number must be less than 1!\n");
         return 1;
      }else{
         theMaxValue = customMax;
      }
   }

   int target = rand() % (theMaxValue + 1);

   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
   //
   printf("OK cat, I'm thinking of a number from 1 to %d. Make a guess:\n", theMaxValue );
   int aGuess;
   
   do {
      printf( "Enter a number: \n" );
      scanf( "%d", &aGuess );
      if(aGuess < 1){
         printf("You must enter a number that's >= 1\n");
      }else if(aGuess > theMaxValue){
         printf("You must enter a number that's <= %d\n", theMaxValue);
      }else if(aGuess > target){
         printf("No cat... the number I'm thinking of is smaller than %d\n", aGuess);
      }else if(aGuess < target){
         printf("No cat... the number I'm thinking of is larger than %d\n", aGuess);
      }
   } while (aGuess != target);

   printf("You got me.\n");

   printf(" /|_/|\n( o.o )\n > ^ <\n");

   return 0;
}

